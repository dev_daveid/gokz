/*
    Menu to jumper's jumpstat PB's
*/

int iChoosenTargetSteamID32[MAXPLAYERS+1];
int iChoosenMode[MAXPLAYERS+1];
int iChoosenJumpType[MAXPLAYERS+1];
char cTargetName[MAXPLAYERS+1][MAX_NAME_LENGTH];


// =====[ MENUS ]=====

public Action DisplayJsPBMenu(int client)
{
    char cTargetBuffer[32], szBuffer[32], szBuffer2[32], szSteamIDBuffer[64];

    Menu menu = new Menu(DisplayJsPBMenu_Handler);
    menu.SetTitle("Select target:");

    for (int i = 1; i <= GetClientCount(true); i++)
    {
        if (!IsFakeClient(i)&&IsClientInGame(i))
        {
            GetClientName(i, cTargetBuffer, sizeof(cTargetBuffer));
            int iSteamID32 = GetSteamAccountID(i, true);
            IntToString(iSteamID32, szSteamIDBuffer, sizeof(szSteamIDBuffer));
            Format(szBuffer, sizeof(szBuffer), "%s", cTargetBuffer);
            Format(szBuffer2, sizeof(szBuffer2), "%s", szSteamIDBuffer);
            menu.AddItem(szBuffer2, szBuffer);
        }
    }

    menu.ExitButton = true;
    menu.Display(client, MENU_TIME_FOREVER);
    return Plugin_Handled;
}

public Action ModeMenu(int client, int args)
{
    char query[256];

    iChoosenTargetSteamID32[client] = args;
    Format(query, sizeof(query), sql_players_getname, iChoosenTargetSteamID32[client]);
    gH_DB.Query(SQL_GetName, query, client, DBPrio_High);
    
    Menu menu = new Menu(ModeMenu_Handler);
    menu.SetTitle("Select mode:");
    for (int i = 0; i < 3; i++)
    {
        menu.AddItem(gC_ModeNames[i], gC_ModeNames[i]);
    }

    menu.ExitButton = true;
    menu.Display(client, MENU_TIME_FOREVER);
    
    return Plugin_Handled;
}

public Action JumpTypeMenu(int client)
{
    char szBuffer[32];

    Menu menu = new Menu(JumpTypeMenu_Handler);

    for (int i = 0; i < 9; i++)
    {
        Format(szBuffer, sizeof(szBuffer), "%i", i);
        menu.AddItem(szBuffer, gC_JumpTypes[i]);
    }

    menu.ExitButton = true;
    menu.Display(client, MENU_TIME_FOREVER);
    return Plugin_Handled;
}

public Action JumpPbMenu(int client)
{
    char szQuery[256];

    Format(szQuery, sizeof(szQuery), sql_jumpstats_getpb, iChoosenJumpType[client], iChoosenMode[client], iChoosenTargetSteamID32[client]);
    gH_DB.Query(SQL_GetJumpPB, szQuery, client, DBPrio_High);

    return Plugin_Handled;
}

// =====[ MENU HANDLERS ]=====

public int DisplayJsPBMenu_Handler(Menu menu, MenuAction action, int client, int option)
{
    char szBufferSteamID[64];

    if (action == MenuAction_Select)
    {
        iChoosenTargetSteamID32[client] = GetSteamAccountID(option+1, true);
        menu.GetItem(option, szBufferSteamID, sizeof(szBufferSteamID));
        int iSteamID32 = StringToInt(szBufferSteamID);
        ModeMenu(client, iSteamID32);
    } 
    else if (action == MenuAction_End)
    {
        delete menu;
    }
}

public int ModeMenu_Handler(Menu menu, MenuAction action, int client, int option)
{
    if (action == MenuAction_Select)
    {
        iChoosenMode[client] = option;
        JumpTypeMenu(client);
    } 
    else if (action == MenuAction_Cancel)
    {
        DisplayJsPBMenu(client);
    } 
    else if (action == MenuAction_End)
    {
        delete menu;
    }
}

public int JumpTypeMenu_Handler(Menu menu, MenuAction action, int client, int option)
{
    if (action == MenuAction_Select)
    {
        iChoosenJumpType[client] = option;
        JumpPbMenu(client);
    } 
    else if (action == MenuAction_Cancel) {
        ModeMenu(client, iChoosenTargetSteamID32[client]);
    }
    else if (action == MenuAction_End)
    {
        delete menu;
    }
}

public int JumpPbMenu_Handler(Menu menu, MenuAction action, int client, int option)
{
    if (action == MenuAction_Cancel)
    {
        JumpTypeMenu(client);
    } 
    else if (action == MenuAction_End)
    {
        delete menu;
    }
}





public void SQL_GetJumpPB(Database db, DBResultSet results, const char[] error, any data)
{
    float distance, sync, max, pre;
    int strafes, client = data;
    char szDistance[32], szSync[32], steamID[32], szStrafes[32], szSteamID[64], szName[32], date[32], szPre[32], szMax[32], szDate[64];

    if (db == null || results == null)
    {
        GOKZ_PrintToChat(client, true, "%t", "No results found or DB failed!");
        SetFailState(error);
        return;
    }

    Menu menu = new Menu(JumpPbMenu_Handler);
    menu.SetTitle("PB - %s - %s", gC_JumpTypes[iChoosenJumpType[client]], gC_ModeNames[iChoosenMode[client]]);

    GetClientAuthId(client, AuthId_Steam2, steamID, sizeof(steamID));

    while(results.FetchRow())
    {
        distance = (results.FetchInt(4))*0.0001;
        strafes = results.FetchInt(7);
        sync = (results.FetchInt(8))*0.01;
        pre = (results.FetchInt(9))*0.01;
        max = (results.FetchInt(10))*0.01;
        results.FetchString(12, date, sizeof(date));
        Format(szName, sizeof(szName), "Name: %s", cTargetName[client]);
        Format(szSteamID, sizeof(szSteamID), "SteamID3: [U:1:%i]", iChoosenTargetSteamID32[client]);
        Format(szDistance, sizeof(szDistance), "Distance: %0.4f", distance);
        Format(szStrafes, sizeof(szStrafes), "Strafes: %i", strafes);
        Format(szSync, sizeof(szSync), "Sync: %0.1f %", sync);
        Format(szPre, sizeof(szPre), "Pre: %0.2f", pre);
        Format(szMax, sizeof(szMax), "Max: %0.1f", max);
        Format(szDate, sizeof(szDate), "Date: %s", date);
        menu.AddItem("name", szName, ITEMDRAW_DISABLED);
        menu.AddItem("steamID", szSteamID, ITEMDRAW_DISABLED);
        menu.AddItem("distance", szDistance, ITEMDRAW_DISABLED);
        menu.AddItem("strafes", szStrafes, ITEMDRAW_DISABLED);
        menu.AddItem("sync", szSync, ITEMDRAW_DISABLED);
        menu.AddItem("pre", szPre, ITEMDRAW_DISABLED);
        menu.AddItem("max", szMax, ITEMDRAW_DISABLED);
        menu.AddItem("date", szDate, ITEMDRAW_DISABLED);
    }

    menu.ExitButton = true;
    menu.Display(client, MENU_TIME_FOREVER);
}

public void SQL_GetName(Database db, DBResultSet results, const char[] error, any data)
{
    int client = data;
    
    if (db == null || results == null)
    {
        GOKZ_PrintToChat(client, true, "%t", "No results found or DB failed!");
        SetFailState(error);
        return;
    }

    char szNameBuffer[32];
    while (results.FetchRow())
    {
        results.FetchString(1, szNameBuffer, sizeof(szNameBuffer));
        cTargetName[client] = szNameBuffer;
    }
}